extends KinematicBody2D
class_name Player

signal player_health_changed(new_health)
signal player_stamina_changed(new_stamina)

onready var health_stat = $Health
onready var weapon_manager = $WeaponManager
onready var hit_audio = $HitAudio

export (int) var speed = 200

const MAX_STAMINA = 100

var velocity = Vector2()
var team = "Player"
var stamina = 100

func move_input(input):
	look_at(get_global_mouse_position())
	velocity = Vector2()
	if input.is_action_pressed("up"):
		if input.is_action_pressed("run") and stamina != 0:
			velocity.y = -1.8
			decrease_stamina()
		else:
			velocity.y = -1
	if input.is_action_pressed('down'):
		if input.is_action_pressed("run") and stamina != 0:
			velocity.y = 1.8
			decrease_stamina()
		else:
			velocity.y = 1
	if input.is_action_pressed('right'):
		if input.is_action_pressed("run") and stamina != 0:
			velocity.x = 1.8
			decrease_stamina()
		else:
			velocity.x = 1
	if input.is_action_pressed('left'):
		if input.is_action_pressed("run") and stamina != 0:
			velocity.x = -1.8
			decrease_stamina()
		else:
			velocity.x = -1
		
	velocity = velocity * speed

func decrease_stamina():
	if stamina != 0:
		var new_stamina = stamina - 1
		stamina = clamp(new_stamina, 0, 100)
		emit_signal("player_stamina_changed", stamina)
	
func increase_stamina():
	if stamina != 100 and not Input.is_action_pressed("run"):
		stamina += 0.5
		emit_signal("player_stamina_changed", stamina)

func _physics_process(delta):
	move_input(Input)
	move_and_slide(velocity)
	increase_stamina()

func handle_hit_bullet():
	health_stat.health -= 10
	emit_signal("player_health_changed", health_stat.health)
	hit_audio.play()
	if health_stat.health == 0:
		get_tree().change_scene(str("res://Scenes/UI/DeathMenu.tscn"))

func handle_hit_shell():
	health_stat.health = 0
	get_tree().change_scene(str("res://Scenes/UI/DeathMenu.tscn"))

func set_health(value):
	health_stat.health = value
	emit_signal("player_health_changed", health_stat.health)

func get_health():
	return health_stat.health
