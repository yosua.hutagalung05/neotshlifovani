extends Area2D



func _on_HealthPickup_body_entered(body):
	if body.is_in_group("player") and body.get_health() != 100:
		body.set_health(100)
		queue_free()
