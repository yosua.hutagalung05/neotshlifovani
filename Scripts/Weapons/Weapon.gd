extends Node2D

export (PackedScene) var GunBullet
export (bool) var semi_auto = false

var team = ""

onready var gun_muzzle = $GunMuzzle
onready var shoot_cooldown = $ShootCooldown
onready var muzzle_flash_animator = $MuzzleFlashAnimator
onready var muzzle_flash = $MuzzleFlash
onready var audio_player = $Audio
onready var audio_player2 = $Audio2
onready var audio_player3 = $Audio3


func _ready():
	muzzle_flash.hide()
	

func initialize(team):
	self.team = team

func shoot():
	if shoot_cooldown.is_stopped() and GunBullet != null:
		var gun_bullet_instance = GunBullet.instance()
		var direction = (gun_muzzle.global_position - global_position).normalized()
		GlobalSignals.emit_signal("bullet_fired", gun_bullet_instance, gun_muzzle.global_position, direction, team)
		shoot_cooldown.start()
		muzzle_flash_animator.play("muzzle_flash")
		if audio_player.playing:
			audio_player2.play()
		elif audio_player2.playing:
			audio_player3.play()
		else:
			audio_player.play()
		
