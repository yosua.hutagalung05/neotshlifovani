extends Node2D

export (PackedScene) var TankShell
export (bool) var semi_auto = false

var team = ""
var ammo = 5

onready var launcher_muzzle = $LauncherMuzzle
onready var shoot_cooldown = $ShootCooldown
onready var audio_player = $Audio
	
func initialize(team):
	self.team = team

func shoot():
	if shoot_cooldown.is_stopped() and TankShell != null and ammo != 0:
		var tank_shell_instance = TankShell.instance()
		var direction = (launcher_muzzle.global_position - global_position).normalized()
		GlobalSignals.emit_signal("bullet_fired", tank_shell_instance, launcher_muzzle.global_position, direction, team)
		shoot_cooldown.start()
		ammo -= 1
		audio_player.play()
