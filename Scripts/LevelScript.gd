extends Node2D

onready var bullet_manager = $BulletManager
onready var explosion_manager = $ExplosionManager
onready var hud = $HUD
onready var player = $Player
onready var bgm_player = $BGM

func _ready():
	GlobalSignals.connect("bullet_fired", bullet_manager, "handle_bullet_spawned")
	GlobalSignals.connect("shell_hit", explosion_manager, "show_explosion")
	player.connect("player_health_changed", hud, "set_health_count")
	player.connect("player_stamina_changed", hud, "set_stamina_count")
	bgm_player.play()
