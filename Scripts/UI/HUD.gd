extends CanvasLayer

onready var health_count = $MarginContainer/VBoxContainer/VBoxContainer/HBoxContainer/HealthCount
onready var stamina_count = $MarginContainer/VBoxContainer/VBoxContainer/HBoxContainer2/StaminaCount


func set_health_count(new_health):
	health_count.text = str(new_health)

func set_stamina_count(new_stamina):
	stamina_count.text = str(new_stamina)
