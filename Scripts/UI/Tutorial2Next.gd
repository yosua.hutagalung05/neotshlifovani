extends LinkButton

func _on_LinkButton_pressed():
	var tutorial2 = get_parent().get_parent()
	var tutorial3 = get_parent().get_parent().get_parent().get_node("Tutorial3")
	tutorial2.hide()
	tutorial3.show()
