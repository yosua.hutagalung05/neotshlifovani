extends Node2D

onready var animator = $Sprite/AnimationPlayer
onready var timer = $Timer

func _process(delta):
	if timer.is_stopped():
		queue_free()
