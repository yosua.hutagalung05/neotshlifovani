extends Area2D

export (int) var speed = 10
export (PackedScene) var ExplosionAnim
export (PackedScene) var ExplosionAudio

var direction = Vector2.ZERO
var team = ""

func _physics_process(delta: float):
	if direction != Vector2.ZERO:
		var velocity = direction * speed
		global_position += velocity

func set_direction(direction: Vector2):
	self.direction = direction
	rotation += direction.angle()

# Remove bullet if exited screen
func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

func _on_TankShell_body_entered(body):
	var explosion_audio_instance = ExplosionAudio.instance()
	if body.has_method("handle_hit_shell") and body.is_in_group("objects"):
		body.handle_hit_shell()
		var explosion_anim_instance = ExplosionAnim.instance()
		GlobalSignals.emit_signal("shell_hit", self, global_position, explosion_anim_instance, explosion_audio_instance)
		print(team)
		queue_free()
	elif body.has_method("handle_hit_shell") and body.team != self.team:
		body.handle_hit_shell()
		var explosion_anim_instance = ExplosionAnim.instance()
		GlobalSignals.emit_signal("shell_hit", self, global_position, explosion_anim_instance, explosion_audio_instance)
		queue_free()
	else:
		var explosion_anim_instance = ExplosionAnim.instance()
		GlobalSignals.emit_signal("shell_hit", self, global_position, explosion_anim_instance, explosion_audio_instance)
		queue_free()
