extends Area2D
class_name GunBullet

export (int) var speed = 10

var direction = Vector2.ZERO
var team = ""

func _physics_process(delta: float):
	if direction != Vector2.ZERO:
		var velocity = direction * speed
		global_position += velocity

func set_direction(direction):
	self.direction = direction
	rotation += direction.angle()

# Remove bullet if exited screen
func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

func _on_GunBullet_body_entered(body):
	if body.has_method("handle_hit_bullet") and body.is_in_group("objects"):
		body.handle_hit_bullet()
		queue_free() 
	elif body.has_method("handle_hit_bullet") and body.team != self.team:
		body.handle_hit_bullet()
		queue_free() 
	else:
		queue_free()
