extends Node

signal bullet_fired(bullet, position, direction, team)
signal shell_hit(shell, position, anim, audio)
