extends StaticBody2D

onready var health_stat = $Health

func handle_hit_bullet():
	pass

func handle_hit_shell():
	health_stat.health -= 50
	if health_stat.health == 0:
		queue_free()
