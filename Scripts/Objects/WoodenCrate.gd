extends StaticBody2D

onready var health_stat = $Health

func handle_hit_bullet():
	health_stat.health -= 10
	if health_stat.health == 0:
		queue_free()

func handle_hit_shell():
	health_stat.health = 0
	queue_free()
