extends Node2D

export (int) var health = 100 setget set_health

func set_health(value):
	health = clamp(value, 0, 100)
