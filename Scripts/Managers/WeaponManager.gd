extends Node2D

onready var current_weapon = $Pistol

var weapons = []

func _ready():
	weapons = get_children()
	
	for weapon in weapons:
		weapon.hide()
		weapon.team = "Player"
	
	current_weapon.show()

func _process(delta):
	if not current_weapon.semi_auto and Input.is_action_pressed("shoot"):
		current_weapon.shoot()
		
	
func get_current_weapon():
	return current_weapon

func switch_weapon(weapon):
	if weapon == current_weapon:
			return
		
	current_weapon.hide()
	weapon.show()
	current_weapon = weapon

func _unhandled_input(event):
	if current_weapon.semi_auto and event.is_action_released('shoot'):
		current_weapon.shoot()
	elif event.is_action_released('weapon_1'):
		switch_weapon(weapons[0])
	elif event.is_action_released('weapon_2'):
		switch_weapon(weapons[1])
	elif event.is_action_released('weapon_3'):
		switch_weapon(weapons[2])
