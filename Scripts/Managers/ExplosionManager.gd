extends Node

func show_explosion(shell, position, anim, audio):
	add_child(anim)
	add_child(audio)
	anim.global_position = position
	audio.global_position = position
	anim.animator.play("explode")
	audio.audio_player.play()
