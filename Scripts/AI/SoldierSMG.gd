extends KinematicBody2D

onready var ai = $AI
onready var health_stat = $Health
onready var weapon = $SMG
onready var hit_audio = $HitAudio

var team = "Enemy"

func _ready():
	ai.initialize(self, weapon)
	weapon.initialize(team)

func handle_hit_bullet():
	health_stat.health -= 20
	print("Enemy hit")
	print(health_stat.health)
	hit_audio.play()
	if health_stat.health == 0:
		queue_free()

func handle_hit_shell():
	queue_free()
