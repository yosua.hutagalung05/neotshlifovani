extends Node2D

signal state_changed(new_state)

onready var player_detection_zone = $PlayerDetectionZone

enum State{
	IDLE,
	ENGAGE
}

var current_state = State.IDLE setget set_state
var actor = null
var player = null
var weapon = null
var origin = global_position

func _process(delta):
	match current_state:
		State.IDLE:
			pass
		State.ENGAGE:
			if player != null and weapon != null:
				var angle_to_player = actor.global_position.direction_to(player.global_position).angle()
				actor.rotation = lerp(actor.rotation, angle_to_player, 0.1)
				if abs(actor.rotation - angle_to_player) < 0.1:
					weapon.shoot()
			else:
				print("Engage but now weapon")
		_:
			print("Error")

func initialize(actor, weapon):
	self.actor = actor
	self.weapon = weapon

func set_state(new_state):
	if new_state == current_state:
		return
	current_state = new_state
	
	emit_signal("state_changed", current_state)

func _on_PlayerDetectionZone_body_entered(body):
	if body.is_in_group("player"):
		player = body
		set_state(State.ENGAGE)
