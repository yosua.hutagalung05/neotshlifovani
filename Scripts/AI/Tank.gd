extends StaticBody2D

onready var health_stat = $Health

var team = "Enemy"

func handle_hit_shell():
	health_stat.health -= 50
	if health_stat.health == 0:
		queue_free()
