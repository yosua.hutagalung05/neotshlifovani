extends Node2D

onready var tank_ai = $TankAI

export (PackedScene) var TankShell

var team = "Enemy"

onready var barrel_muzzle = $BarrelMuzzle
onready var barrel_center = $BarrelCenter
onready var shoot_cooldown = $ShootCooldown
onready var barrel_flash_animator = $BarrelFlashAnimator
onready var barrel_flash = $BarrelFlash
onready var fire_audio = $FireAudio

func _ready():
	tank_ai.initialize(self)
	barrel_flash.hide()

func shoot():
	if shoot_cooldown.is_stopped() and TankShell != null:
		print("A")
		var tank_shell_instance = TankShell.instance()
		var direction = (barrel_muzzle.global_position - barrel_center.global_position).normalized()
		GlobalSignals.emit_signal("bullet_fired", tank_shell_instance, barrel_muzzle.global_position, direction, team)
		shoot_cooldown.start()
		barrel_flash_animator.play("barrel_flash")
		fire_audio.play()
